package ru.tsc.kitaev.tm.controller;

import ru.tsc.kitaev.tm.api.controller.IProjectController;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.service.ProjectService;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name,description);
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW TASKS]");
        final List<Project> tasks= projectService.findAll();
        for (Project project: tasks) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR TASKS]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
