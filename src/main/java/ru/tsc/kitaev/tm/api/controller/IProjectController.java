package ru.tsc.kitaev.tm.api.controller;

public interface IProjectController {

    void createProjects();

    void showProjects();

    void clearProjects();

}
