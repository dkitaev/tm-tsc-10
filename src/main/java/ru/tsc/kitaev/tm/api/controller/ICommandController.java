package ru.tsc.kitaev.tm.api.controller;

import ru.tsc.kitaev.tm.model.Command;

public interface ICommandController {

    void showAbout();

    void exit();

    void showArguments();

    void showCommands();

    void showErrorArgument();

    void showErrorCommand();

    void showHelp();

    void showInfo();

    void showVersion();
}
