package ru.tsc.kitaev.tm.api.controller;

public interface ITaskController {

    void createTasks();

    void showTasks();

    void clearTasks();

}
