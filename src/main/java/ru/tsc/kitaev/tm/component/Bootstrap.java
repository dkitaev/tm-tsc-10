package ru.tsc.kitaev.tm.component;

import ru.tsc.kitaev.tm.api.controller.ICommandController;
import ru.tsc.kitaev.tm.api.controller.IProjectController;
import ru.tsc.kitaev.tm.api.controller.ITaskController;
import ru.tsc.kitaev.tm.api.repository.ICommandRepository;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.ICommandService;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.constant.ArgumentConst;
import ru.tsc.kitaev.tm.constant.TerminalConst;
import ru.tsc.kitaev.tm.controller.CommandController;
import ru.tsc.kitaev.tm.controller.ProjectController;
import ru.tsc.kitaev.tm.controller.TaskController;
import ru.tsc.kitaev.tm.repository.CommandRepository;
import ru.tsc.kitaev.tm.repository.ProjectRepository;
import ru.tsc.kitaev.tm.repository.TaskRepository;
import ru.tsc.kitaev.tm.service.CommandService;
import ru.tsc.kitaev.tm.service.ProjectService;
import ru.tsc.kitaev.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

}
